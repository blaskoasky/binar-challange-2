package com.blaskoasky.main;

import com.blaskoasky.harusadaoopnya.IMainMenu;

import java.util.Scanner;

public class Menu implements IMainMenu {

    Scanner scan = new Scanner(System.in);
    MeanMedMod mmm = new MeanMedMod();
    String csvPath = "src/file/data_sekolah.csv";
    String saveMeanMedianModusPath = "src/file/Mean_Median_Modus_DataSekolah.txt";
    String saveModusSekolahPath = "src/file/Modus_Sekolah.txt";

    @Override
    public void switchMenu() {

        switch (mainMenu()) {
            case 1:
                mmm.write(saveMeanMedianModusPath);
                // menuBack();
                break;
            case 2:
                mmm.writeMod(saveModusSekolahPath);
                // menuBack();
                break;
            case 3:
                mmm.write(saveMeanMedianModusPath);
                mmm.writeMod(saveModusSekolahPath);
                // menuBack();
                break;
            case 0:
                System.out.println("Keluar Console....");
                System.exit(0);
                break;
            default:
                System.out.println("\n");
                System.out.println("\n");
                System.out.println("PILIH ANGKA YANG BENAR!!!");
                System.out.println("PILIH ANGKA YANG BENAR!!!");
                System.out.println("PILIH ANGKA YANG BENAR!!!");
                System.out.println("PILIH ANGKA YANG BENAR!!!");
                System.out.println("PILIH ANGKA YANG BENAR!!!");
                System.out.println("\n");
                System.out.println("\n");
                switchMenu();
                break;

        }
    }

    @Override
    public int mainMenu() {
        System.out.println("=================================================");
        System.out.println("Aplikasi Mengolah Data Nilai Siswa");
        System.out.println("-------------------------------------------------");
        System.out.println("1. Menghitung Mean-Median-Modus");
        System.out.println("2. Menghitung Modus Sekolah");
        System.out.println("3. Jalankan dua program diatas");
        System.out.println("0. Exit");
        System.out.println("=================================================");

        System.out.print("Masukkan Pilihan: ");
        var pilih = scan.nextInt();
        return pilih;
    }

    /*@Override
    public void menuBack() {
        System.out.println("=================================================");
        System.out.println("0. Exit");
        System.out.println("1. Kembali ke menu utama");
        System.out.print("Masukkan Pilihan: ");
        var pilih = scan.nextInt();

        switch (pilih) {
            case 0:
                System.out.println("Keluar Console....");
                System.exit(0);
                break;
            case 1:
                switchMenu();
                break;
            default:
                System.out.println("\n");
                System.out.println("\n");
                System.out.println("PILIH ANGKA YANG BENAR!!!");
                System.out.println("PILIH ANGKA YANG BENAR!!!");
                System.out.println("PILIH ANGKA YANG BENAR!!!");
                System.out.println("PILIH ANGKA YANG BENAR!!!");
                System.out.println("PILIH ANGKA YANG BENAR!!!");
                System.out.println("\n");
                System.out.println("\n");
                switchMenu();
                break;
        }
    }*/
}
